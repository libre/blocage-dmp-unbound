# Blocage des DMP avec Unbound

Ce projet regroupe la configuration nécessaire pour Unbound afin de bloquer certaines « Data Management Platforms » (DMP) utilisées par de plus en plus de sites (liberation.fr, oui.scnf, lemonde.fr, fnac.com...) et qui échappent – pour l'instant aux bloqueurs de traqueurs traditionnels (uBlock Origin ou uMatrix par exemple) ou [un peu plus sophistiqué](https://framagit.org/Shaft/unbound-adblock).

La première partie de ce README est assez didactique et revient sur les mécanismes en jeu (j'avais la flemme d'écrire un billet de blog alors je mets ça là :D). Pour passer directement à la partie configuration, [c'est tout en bas](#principe).

[La liste des domaines bloqués](liste-domaine.txt)

- [DMP ?](#DMP-)
- [Le cœur du problème](#le-cœur-du-problème)
- [La fourberie](#la-fourberie)
	- [DNS](#DNS)
	- [CNAME](#CNAME)
	- [En quoi c'est efficace ?](#en-quoi-cest-efficace-)
- [Bloquer les indésirables avec Unbound](#bloquer-les-indésirables-avec-unbound)
	- [Principe](#principe)
	- [Déploiement (Unbound 1.7.0+)](#déploiement-unbound-170)
	- [Déploiement (Unbound < 1.7.0)](#déploiement-unbound-170-1)
- [Sources](sources)
- [LICENCE](#licence)

## DMP ?

Ces plateformes fournissent cookies et mouchards afin de collecter, pour le compte de leurs clients, vos données personnelles et ensuite en faire on se sait trop quoi (des statistiques d'utilisation, mais très certainement aussi des choses liées à du ciblage publicitaire)

## Le cœur du problème

Dernièrement, une conjoncture d'éléments est venue semer le trouble chez nos aspirateurs :
- L'entrée en vigueur du RGPD en Europe
- La part de plus en plus grande d'internautes utilisant des bloqueurs de publicités
- La volonté des éditeurs de navigateurs web (Chrome et Firefox – et donc leurs dérivés) de bloquer par défaut les traqueurs ou les cookies tiers.

Un mouchard est dit tiers – ou *3rd-party* quand on est disruptif – quand il n'est pas chargé depuis le domaine (ou un de ses sous domaines) que vous visitez. Par exemple quand on visite [https://liberation.fr/](https://liberation.fr/), le script chargé depuis `www.google-analytics.com` est tiers. Si le même mouchard est chargé depuis un sous-domaine de `liberation.fr`, il est dit *1st-party* (primaire a priori en bon français).

Le hic, pour nos amis start-uppers est que les mouchards/cookies tiers sont triviaux à bloquer. Ne pas charger les cookies tiers est d'ailleurs une option répandue dans les navigateurs depuis des années.

## La fourberie

La technique développée par les marketeux pour ne pas voir les juteuses données personnelles leur échapper est à la fois techniquement très simple et redoutable. Il suffit de transformer les crasses *3rd-party* en *1st-party* afin de les cacher sous le cyber-tapis pour reprendre l'expression de Reflets. Et pour se faire passer par le DNS et ses possiblités.

### DNS

Rappelons très rapidement que le DNS est un système (le principal et le plus utilisé) permettant d'associer un nom de domaine à des données via ce que l'on appelle des enregistrements. En général, on associe des noms de domaine à des adresses IP (enregistrements `A` et `AAAA`, car les machines ont bien besoin d'une adresse pour savoir où envoyer leur paquets), mais pas que : on peut mettre du texte quelconque dans le DNS (enregistrement `TXT`), ou bien dire qu'un nom de domaine est en fait un « pseudonyme » (enregistrement `CNAME`). C'est cela que nos vaillants pourfendeurs d'intimités ont choisi.

L'ensemble des noms de domaines forme une espèce de gigantesque annuaire décentralisé (mais pas a-centré :) ) que l'on consulte avec un logiciel appelé résolveur (car trouver les données associées à un nom de domaine revient à « résoudre » ce nom). Le résolveur est le plus souvent un service fourni par le FAI, mais rien n'empêche d'un installer un sur sa machine. Il existe plusieurs très bon logiciel pour cela tel que Unbound ou Knot-Resolver (ce projet s'intéresse à Unbound car c'est celui que je maitrîse le plus)

### CNAME

`CNAME` est l'acronyme anglais de *Canonical NAME*, nom canonique. Dans la [terminologie du DNS](https://www.bortzmeyer.org/8499.html), si Batman est l'alias de Bruce Wayne, alors Bruce Wayne est le nom canonique de Batman. Si l'on représentait cela sous la forme d'un enregistrement DNS, on aurait quelque chose du type :
```
bat.man IN CNAME bruce.wayne
```

Le but est d'associer les adresses IPs non pas à `bat.man` mais à `bruce.wayne`.

Pour prendre un exemple réel, par exemple si l'on souhaite se connecter au site des Impôts (`www.impots.gouv.fr`), votre navigateur va demander l'adresse IP associée à ce domaine au résolveur configuré dans votre système. Le résolveur va alors demander chercher cette adresse IP, mais se verra d'abord répondre que `www.impots.gouv.fr` est l'alias de `cs1003.wpc.omicroncdn.net.` et que l'adresse IP de ce domaine est `152.199.19.61` (domaine et IP appartenants à EdgeCast, une filiale de Verizon, géant des télécoms US). Le résolveur va donc donner cette adresse au navigateur sans mentionner cette histoire de `CNAME` (car le navigateur s'en fout).

Et nos traqueurs dans tout ça ? Eh bien c'est là que ces alias rentrent en jeu. Via leur utilisation, les nuisibles arrivent à se dissimuler car l'opération est invisible pour le navigateur.

Dans le cas de la SNCF (`oui.sncf`), au lieu de charger un traqueur depuis un sous-domaine de leur prestataire Eulerian (`eulerian.net`), ils vont imbriquer des CNAME. Sur le site de SNCF, les traqueurs d'Eulerian sont chargés depuis le domaine `v.oui.sncf`, mais ce dernier est l'alias de `voyages-sncf.eulerian.net.` qui est lui-même l'alias de `vsc.eulerian.net.`. Mais votre navigateur ne verra que `v.oui.sncf` et sera ainsi berné (et il n'est pas le seul, votre bloqueur de pubs se fait aussi avoir).

### En quoi c'est efficace ?

La fourberie est *très* fourbe. La contrer demande d'agir directement sur le résolveur DNS que vous utilisiez (d'une manière non conventionnelle), ce qui présuppose que vous utilisez le vôtre. Cela disqualifie 99% de la population. En effet, les techniques usuelles :
- Bloqueur de pub
- Fichier [`hosts`](https://fr.wikipedia.org/wiki/Hosts) (très utilisé sur les bloqueurs de pubs sur mobile type [AdAway](https://f-droid.org/fr/packages/org.adaway) ou [DNS66](https://f-droid.org/fr/packages/org.jak_linux.dns66)
- [Configuration habituelle](https://framagit.org/Shaft/unbound-adblock) d'un résolveur menteur
- Pi-Hole

*sont inefficaces*.

Regardons comment se déroule le mécanisme de résolution, dans le cas de `f7ds.liberation.fr.` (qui est l'alias de `liberation.eulerian.net.`, lui-même étant l'alias de `atc.eulerian.net.`).

En naviguant sur le site de Libération, un script présent dans la page de garde du site va demander de charger une ressource sur `f7ds.liberation.fr.`. Partant de là, le navigateur va demander l'adresse IP de ce domaine au résolveur. Ce dernier va alors :
1. Demander l'adresse de `f7ds.liberation.fr` aux serveurs de noms de `liberation.fr`
2. Se voir répondre par ses serveurs que `liberation.eulerian.net.` et le nom canonique de `f7ds.liberation.fr`
3. Avec cette réponse, il reprend le mécanisme de résolution en prenant pour base `liberation.eulerian.net.` et demander aux serveurs de noms d'Eulerian
4. Ces derniers répondent que `atc.eulerian.net.` est le nom canonique de `liberation.eulerian.net.` et donnent l'adresse IP d'`atc.eulerian.net` dans la foulée (109.232.197.179)

Il est tout à fait possible de faire mentir un résolveur DNS, il suffit de lui dire : « Pour tel domaine (et ses sous-domaines), réponds ça ». Par exemple : « Pour `google-analytics.com` réponds que le domaine n'existe pas ». C'est en général très efficace, sauf dans le cas des `CNAME` (à cause du fonctionnement du mécanisme de résolution spécifique dans ce cas), et il faudrait savoir à l'avance que c'est `f7ds.liberation.fr` qu'il faut bloquer, ce qui est une tâche ardue : il faudrait trouver tous les clients des DMP et trouver quels domaines servent les scripts problématiques.

Pour les bloqueurs comme uMatrix par exemple, ces derniers bloquent par défaut par les domaines tiers. Pour les sous-domaines du domaine visité, les scripts et les cookies ne sont pas bloqués. Il est donc inefficace sauf à le passer en mode *ultra-parano* consistant à bloquer par défaut scripts et cookies pour tous les domaines.

Pour un bloqueur de pubs comme uBlock Origin, comme dans le cas d'un blocage via résolveur menteur, il faudrait également connaître à l'avance le domaine à bloquer (`f7ds.liberation.fr` dans l'exemple vu ci-dessus).


## Bloquer les indésirables avec Unbound

### Principe

On entre dans la partie technique, pour faire court, on va décréter à Unbound que nous contrôlons les domaines indésirables (`eulerian.net` par exemple) afin de faire échouer la résolution DNS. La technique est [développée plus en détail sur mon blog](https://www.shaftinc.fr/escalade-traque-eulerian.html). Pour mémoire :
- On crée un fichier de zone où l'on met un enregistrement `SOA`.
- On l'associe à chaque domaine à bloquer dans la configuration d'Unbound

Cette technique sera valable, pour les versions d'Unbound supérieures à 1.7.0. Si vous utilisez une version une version plus vieille (sous Ubuntu 18.04 LTS par exemple), il faudra passer par une technique un peu moins propre (mais efficace) : on ne déclare plus comme propriétaire du domaine, mais on dit à Unbound que le serveur faisant autorité pour ce dernier est à une adresse bidon (le `localhost` en l'occurence).

### Déploiement (Unbound 1.7.0+)

1. Copier [blocked.zone](blocked.zone) dans `/var/lib/unbound/` (un autre répertoire est valable, mais celui-ci doit être présent sous toutes les distributions a priori)
2. Copier [adblock-war.conf](adblock-war.conf) dans `/etc/unbound/unbound.conf.d/`, (Debian ou dérivé) ou bien ailleurs, tant que `unbound.conf` sait où chercher (ou bien carrément coller le contenu du fichier dans `unbound.conf`)
3. Redémarrer Unbound

Normalement c'est fonctionnel.

```
$ dig v.oui.sncf

; <<>> DiG 9.11.5-P4-5.1+b1-Debian <<>> v.oui.sncf
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 5341
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;v.oui.sncf.                    IN      A

;; ANSWER SECTION:
v.oui.sncf.             3600    IN      CNAME   voyages-sncf.eulerian.net.

;; AUTHORITY SECTION:
eulerian.net.           10800   IN      SOA     localhost. nobody.invalid. 1 3600 1200 604800 10800

;; Query time: 332 msec
;; SERVER: ::1#53(::1)
;; WHEN: mer. nov. 13 02:51:30 CET 2019
;; MSG SIZE  rcvd: 137
```

Pour certains domaines, on a du `SERVFAIL`, ce qui n'est a priori pas bien grave, c'est bloqué et c'est ce qui compte :

```
$ dig degh48.fnac.com

; <<>> DiG 9.11.5-P4-5.1+b1-Raspbian <<>> degh48.fnac.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 34163
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;degh48.fnac.com.               IN      A

;; Query time: 55 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: mer. nov. 13 02:49:52 CET 2019
;; MSG SIZE  rcvd: 44
```

### Déploiement (Unbound < 1.7.0)

1. Copier [adblock-stub.conf](adblock-stub.conf) dans `/etc/unbound/unbound.conf.d/` ou ailleurs
2. Redémarrer Unbound

## Sources

Les sources ayant servi à trouver les domaines à bloquer :
- Le travail d'[Aeris](https://social.imirhil.fr/@aeris).
- [Libération : à traqueur vaillant, rien d'impossible](https://reflets.info/articles/liberation-a-traqueur-vaillant-rien-d-impossible) sur Reflets.
- [Affreux, sales et méchants](https://reflets.info/articles/affreux-sales-et-mechants), toujours sur Reflets.

## Licence

Licence Publique IV : voir [LICENSE](LICENSE) ou https://www.shaftinc.fr/licence-publique-iv.html
